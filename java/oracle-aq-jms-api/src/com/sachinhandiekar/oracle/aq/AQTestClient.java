package com.sachinhandiekar.oracle.aq;

import oracle.AQ.*;
import oracle.jdbc.pool.OracleDataSource;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import java.sql.SQLException;
import java.util.Vector;

public class AQTestClient {

    private static final String queueName = "testik10";

    private static final String queueTable = "testik10";

    private static final String queueOwner = "HO_KLOUCEK_IN";

    public void enqueueMessage(String xmlMessage) throws SQLException, AQException, ClassNotFoundException {
        java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aqsession = null;

        // Register the Oracle AQ Driver
        Class.forName("oracle.AQ.AQOracleDriver");
        try {
            AQEnqueueOption enqueueOption = new AQEnqueueOption();

            aqsession = AQDriverManager.createAQSession(aqconn);
            AQQueue queue = aqsession.getQueue(queueOwner, queueName);
            AQMessage msg = queue.createMessage();

            AQMessageProperty msgProps = new AQMessageProperty();
            msgProps.setPriority(1);
            //Vector recipientList = new Vector();
            //AQAgent subs1 = new AQAgent("Sub2", null, 0);
            //recipientList.add(subs1);
            //msgProps.setRecipientList(recipientList);
            //msg.setMessageProperty(msgProps);

            AQObjectPayload payload = msg.getObjectPayload();

            Object [] test_attributes = new Object[2];
            test_attributes [0] = "AsyncTask";
            test_attributes [1] = "5555";

            StructDescriptor personDesc =
                    StructDescriptor.createDescriptor("HO_KLOUCEK_IN.MESSAGE_TYP", aqconn);

            STRUCT new_async = new STRUCT(personDesc, aqconn, test_attributes);

            payload.setPayloadData(new_async);

            queue.enqueue(enqueueOption, msg);
            aqconn.commit();
            System.out.println("Message succesfully enqueued..");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            aqsession.close();
            aqconn.close();
        }
    }

    public static OracleDataSource getOracleDataSource() throws SQLException {
        OracleDataSource ds = new OracleDataSource();
        ds.setURL("jdbc:oracle:thin:@ldap://ldap.homecredit.net:389/hodev,cn=OracleContext,dc=cz,dc=infra");
        ds.setDatabaseName("HO_KLOUCEK_IN");
        ds.setUser("appdeploy");
        ds.setPassword("HODEV");

        return ds;
    }

    public static void main(String[] args) {

        AQTestClient aqTest = new AQTestClient();

        String xmlMessage = "<asyncTask>565656</asyncTask>";
        try {
            aqTest.enqueueMessage(xmlMessage);
            //aqTest.enqueueMessage(xmlMessage);
            //aqTest.dequeueMessage();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
