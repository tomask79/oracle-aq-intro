package com.sachinhandiekar.oracle.aq;

import oracle.AQ.*;
import oracle.jdbc.pool.OracleDataSource;

import java.sql.SQLException;

import oracle.sql.ORADataFactory;
import oracle.xdb.XMLType;

public class AQTestObject {

    private static final String queueOwner = "HO_KLOUCEK_IN";

    private static final String queueName = "myXMLQueueTest";

    private static final String queueTable = "myXMLQueueTableTest";

    public static void main(String[] args) {

        AQTestObject aqTest = new AQTestObject();

        String xmlMessage = "<asyncTask>565656</asyncTask>";
        try {
            aqTest.createQueue();
            //aqTest.enqueueMessage(xmlMessage);
            //aqTest.dequeueMessage();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createQueue() throws SQLException, AQException, ClassNotFoundException
    {
        AQQueueTableProperty qtable_prop;
        AQQueueProperty queue_prop;
        AQQueueTable q_table;
        AQQueue queue;

        java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aqsession = null;

        // Register the Oracle AQ Driver
        Class.forName("oracle.AQ.AQOracleDriver");
        try {
            AQEnqueueOption enqueueOption = new AQEnqueueOption();

            aqsession = AQDriverManager.createAQSession(aqconn);

            qtable_prop = new AQQueueTableProperty("SYS.XMLTYPE");

 			/* Creating a queue table called aq_table1 in aqjava schema: */
            q_table = aqsession.createQueueTable(queueOwner, queueTable, qtable_prop);
            System.out.println("Successfully created "+queueTable+" in "+queueOwner+" schema");

 			/* Creating a new AQQueueProperty object */
            queue_prop = new AQQueueProperty();

 			/* Creating a queue called aq_queue1 in aq_table1: */
            queue = aqsession.createQueue(q_table, queueName, queue_prop);
            queue.start(true, true);
            System.out.println("Successfully created "+queueName+" in "+queueOwner+"");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            aqsession.close();
            aqconn.close();
        }
    }

    public void enqueueMessage(String xmlMessage) throws SQLException, AQException, ClassNotFoundException {
        java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aqsession = null;

        // Register the Oracle AQ Driver
        Class.forName("oracle.AQ.AQOracleDriver");
        try {
            AQEnqueueOption enqueueOption = new AQEnqueueOption();

            aqsession = AQDriverManager.createAQSession(aqconn);
            AQQueue queue = aqsession.getQueue(queueOwner, queueName);
            AQMessage msg = queue.createMessage();

            AQObjectPayload payload = msg.getObjectPayload();
            XMLType payloadData = XMLType.createXML(aqconn, xmlMessage);
            payload.setPayloadData(payloadData);

            queue.enqueue(enqueueOption, msg);
            aqconn.commit();
            System.out.println("Message succesfully enqueued..");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            aqsession.close();
            aqconn.close();
        }
    }

    public void dequeueMessage() throws AQException, SQLException, ClassNotFoundException {
        java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aq_sess = null;

        Class.forName("oracle.AQ.AQOracleDriver");

        try {
            aq_sess = AQDriverManager.createAQSession(aqconn);

            AQQueue queue;
            AQMessage message;
            AQDequeueOption deq_option;

            queue = aq_sess.getQueue(queueOwner, queueName);

            deq_option = new AQDequeueOption();

            // receive a message via native interface
            ORADataFactory factory = XMLType.getORADataFactory();

            message = queue.dequeue(deq_option, factory);

            if (message == null) {
                System.out.println("no messages");
            }
            else {
                System.out.println("Successful dequeue");

                XMLType messageData = (XMLType) message.getObjectPayload().getPayloadData();
                String xmlString = messageData.getStringVal();
                System.out.println("content: " + xmlString);

                //Commit
                aqconn.commit();

            }
        }
        finally {
            aq_sess.close();
            aqconn.close();
        }
    }

    public static OracleDataSource getOracleDataSource() throws SQLException {
        OracleDataSource ds = new OracleDataSource();
        ds.setURL("jdbc:oracle:thin:@ldap://ldap.homecredit.net:389/hodev,cn=OracleContext,dc=cz,dc=infra");
        ds.setDatabaseName("HO_KLOUCEK_IN");
        ds.setUser("appdeploy");
        ds.setPassword("HODEV");

        return ds;
    }

}

