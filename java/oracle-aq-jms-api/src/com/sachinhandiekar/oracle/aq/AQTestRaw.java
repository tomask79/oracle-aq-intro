package com.sachinhandiekar.oracle.aq;

import java.sql.SQLException;

import oracle.AQ.*;
import oracle.jdbc.pool.OracleDataSource;

public class AQTestRaw {

	private static final String queueOwner = "HO_KLOUCEK_IN";

	private static final String queueName = "myRawQueueTest";

	private static final String queueTable = "myRawQueueTableTest";

	public static void main(String[] args) {

		AQTestRaw aqTest = new AQTestRaw();

		String xmlMessage = "<sample>hello Ty kravo1</sample>";
		try {
			//aqTest.createQueue();
			aqTest.enqueueMessage(xmlMessage);
			aqTest.dequeueMessage();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createQueue() throws SQLException, AQException, ClassNotFoundException
	{
		AQQueueTableProperty     qtable_prop;
		AQQueueProperty          queue_prop;
		AQQueueTable             q_table;
		AQQueue                  queue;

		java.sql.Connection aqconn = getOracleDataSource().getConnection();
		aqconn.setAutoCommit(false);

		AQSession aqsession = null;

		// Register the Oracle AQ Driver
		Class.forName("oracle.AQ.AQOracleDriver");
		try {
			AQEnqueueOption enqueueOption = new AQEnqueueOption();

			aqsession = AQDriverManager.createAQSession(aqconn);

			/* Creating a AQQueueTableProperty object (payload type - RAW): */
			qtable_prop = new AQQueueTableProperty("RAW");

 			/* Creating a queue table called aq_table1 in aqjava schema: */
			q_table = aqsession.createQueueTable(queueOwner, queueTable, qtable_prop);
			System.out.println("Successfully created "+queueTable+" in "+queueOwner+" schema");

 			/* Creating a new AQQueueProperty object */
			queue_prop = new AQQueueProperty();

 			/* Creating a queue called aq_queue1 in aq_table1: */
			queue = aqsession.createQueue(q_table, queueName, queue_prop);
			queue.start(true, true);
			System.out.println("Successfully created "+queueName+" in "+queueOwner+"");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		finally {
			aqsession.close();
			aqconn.close();
		}
	}

	public void enqueueMessage(String xmlMessage) throws SQLException, AQException, ClassNotFoundException {
		java.sql.Connection aqconn = getOracleDataSource().getConnection();
		aqconn.setAutoCommit(false);

		AQSession aqsession = null;

		// Register the Oracle AQ Driver
		Class.forName("oracle.AQ.AQOracleDriver");
		try {
			AQEnqueueOption enqueueOption = new AQEnqueueOption();

			aqsession = AQDriverManager.createAQSession(aqconn);
			AQQueue queue = aqsession.getQueue(queueOwner, queueName);
			AQMessage msg = queue.createMessage();

			AQRawPayload payload = msg.getRawPayload();
			payload.setStream(xmlMessage.getBytes(), xmlMessage.length());

			queue.enqueue(enqueueOption, msg);
			aqconn.commit();
			System.out.println("Message succesfully enqueued..");
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		finally {
			aqsession.close();
			aqconn.close();
		}
	}

	public void dequeueMessage() throws AQException, SQLException, ClassNotFoundException {
		java.sql.Connection aqconn = getOracleDataSource().getConnection();
		aqconn.setAutoCommit(false);

		AQSession aq_sess = null;

		Class.forName("oracle.AQ.AQOracleDriver");

		try {
			aq_sess = AQDriverManager.createAQSession(aqconn);

			AQQueue queue;
			AQMessage message;
			AQDequeueOption deq_option;

			queue = aq_sess.getQueue(queueOwner, queueName);

			deq_option = new AQDequeueOption();

			message = queue.dequeue(deq_option);

			if (message == null) {
				System.out.println("no messages");
			}
			else {
				System.out.println("Successful dequeue");

				System.out.println("message id: " + new String(message.getRawPayload().getBytes()));
				
				//Commit
				aqconn.commit();

			}
		}
		finally {
			aq_sess.close();
			aqconn.close();
		}

	}

	public static OracleDataSource getOracleDataSource() throws SQLException {
		OracleDataSource ds = new OracleDataSource();
		ds.setURL("jdbc:oracle:thin:@ldap://ldap.homecredit.net:389/hodev,cn=OracleContext,dc=cz,dc=infra");
		ds.setDatabaseName("HO_KLOUCEK_IN");
		ds.setUser("appdeploy");
		ds.setPassword("HODEV");

		return ds;
	}

}
