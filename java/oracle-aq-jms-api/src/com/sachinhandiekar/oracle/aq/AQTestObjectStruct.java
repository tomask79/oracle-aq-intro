package com.sachinhandiekar.oracle.aq;

import oracle.AQ.*;
import oracle.jdbc.OracleTypes;
import oracle.jdbc.pool.OracleDataSource;
import oracle.sql.STRUCT;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class AQTestObjectStruct {
    private static final String queueOwner = "HO_KLOUCEK_IN";

    private static final String queueName = "testik10";

    private static final String queueTable = "testik10";

    public static void main(String[] args) {

        AQTestObjectStruct aqTest = new AQTestObjectStruct();

        String xmlMessage = "<asyncTask>565656</asyncTask>";
        try {
            //aqTest.createQueue();
            //aqTest.enqueueMessage(xmlMessage);
            aqTest.dequeueMessage(args[0]);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createQueue() throws SQLException, AQException, ClassNotFoundException
    {
        AQQueueTableProperty qtable_prop;
        AQQueueProperty queue_prop;
        AQQueueTable q_table;
        AQQueue queue;

        java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aqsession = null;

        // Register the Oracle AQ Driver
        Class.forName("oracle.AQ.AQOracleDriver");
        try {
            AQEnqueueOption enqueueOption = new AQEnqueueOption();

            aqsession = AQDriverManager.createAQSession(aqconn);

            qtable_prop = new AQQueueTableProperty("ho_kloucek_in.Message_typ");
            qtable_prop.setMultiConsumer(true);

 			/* Creating a queue table called aq_table1 in aqjava schema: */
            q_table = aqsession.createQueueTable(queueOwner, queueTable, qtable_prop);
            System.out.println("Successfully created "+queueTable+" in "+queueOwner+" schema");

 			/* Creating a new AQQueueProperty object */
            queue_prop = new AQQueueProperty();

 			/* Creating a queue called aq_queue1 in aq_table1: */
            queue = aqsession.createQueue(q_table, queueName, queue_prop);

            queue.start(true, true);
            System.out.println("Successfully created "+queueName+" in "+queueOwner+"");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            aqsession.close();
            aqconn.close();
        }
    }

    /**
    public void enqueueMessage(String xmlMessage) throws SQLException, AQException, ClassNotFoundException {
        java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aqsession = null;

        // Register the Oracle AQ Driver
        Class.forName("oracle.AQ.AQOracleDriver");
        try {
            AQEnqueueOption enqueueOption = new AQEnqueueOption();

            aqsession = AQDriverManager.createAQSession(aqconn);
            AQQueue queue = aqsession.getQueue(queueOwner, queueName);
            AQMessage msg = queue.createMessage();
            AQObjectPayload payload = msg.getObjectPayload();

            Object [] test_attributes = new Object[2];
            test_attributes [0] = "AsyncTask";
            test_attributes [1] = "121212";

            StructDescriptor personDesc =
                    StructDescriptor.createDescriptor("HO_KLOUCEK_IN.MESSAGE_TYP", aqconn);

            STRUCT new_person = new STRUCT(personDesc, aqconn, test_attributes);

            payload.setPayloadData(new_person);

            queue.enqueue(enqueueOption, msg);
            aqconn.commit();
            System.out.println("Message succesfully enqueued..");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            aqsession.close();
            aqconn.close();
        }
    }
     */

    public void dequeueMessage(final String subsName) throws AQException, SQLException, ClassNotFoundException {
        java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aq_sess = null;

        Class.forName("oracle.AQ.AQOracleDriver");

        try {
            aq_sess = AQDriverManager.createAQSession(aqconn);

            AQQueue queue;
            AQMessage message;
            AQDequeueOption deq_option;

            queue = aq_sess.getQueue(queueOwner, queueName);

            // add subscription
            AQAgent subs = new AQAgent(subsName, null, 0);
            //queue.removeSubscriber(subs, false);
            queue.addSubscriber(subs,null);


            AQDequeueOption opt = new AQDequeueOption();
            opt.setConsumerName(subsName);

            while (true) {
                System.out.println("Waiting on subscription: " + subsName);
                message = queue.dequeue(opt, oracle.sql.STRUCT.class);

                if (message == null) {
                    System.out.println("no messages");
                } else {
                    System.out.println("Successful dequeue");

                    if (message.getObjectPayload().getPayloadData() instanceof STRUCT) {
                        STRUCT popedStruct = (STRUCT) message.getObjectPayload().getPayloadData();
                        System.out.println("subject: " + popedStruct.getAttributes()[0]);
                        System.out.println("text: " + popedStruct.getAttributes()[1]);
                    }

                    //Commit
                    aqconn.commit();
                }
            }
        }
        finally {
            aq_sess.close();
            aqconn.close();
        }
    }

    public static OracleDataSource getOracleDataSource() throws SQLException {
        OracleDataSource ds = new OracleDataSource();
        ds.setURL("jdbc:oracle:thin:@ldap://ldap.homecredit.net:389/hodev,cn=OracleContext,dc=cz,dc=infra");
        ds.setDatabaseName("HO_KLOUCEK_IN");
        ds.setUser("appdeploy");
        ds.setPassword("HODEV");

        return ds;
    }
}
