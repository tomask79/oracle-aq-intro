# Oracle Advance Queuing, is it for you or not? #

Recently in work, in [EmbedIT](https://embedit.cz/), I needed to evaluate whether Oracle AQ is a good choice as replacement for our old asynchronous 
task management system. So let me share my experience with that. First off all, documentation regarding Oracle AQ is pretty huge so I will point out
the most important stuff you want to know.

## Oracle AQ, when to consider? ##

If for some reason you don't want to mess around with Big Data frameworks like [Hazelcast](https://hazelcast.com/) or [Terracotta](http://www.terracotta.org/)
or your current messaging solution through JMS or AMPQ is not enough for you then definitely check: [Oracle Advance Queuing](https://docs.oracle.com/cd/B10501_01/appdev.920/a96587/qintro.htm)

Oracle comes here with own messaging which works through database. When it is suitable for you?

- You've got millions of messages per day.
- You've got huge messages.
- You want to communicate with other databases/applications in messaging manner.
- Oracle AQ is a fantastic thing for distributed transactions. Oracle clearly states:

```
The way Oracle AQ works is that a message is not considered dequeued (and therefore removed , assuming that you are dequeuing in the default destructive mode) 
until all consumers specified by the subscriber list or the override recipients list have dequeued that message.
```

If processing some of your events involves multiple systems and your master application needs some mechanism to know that
processing in one of them failed then Oracle AQ is a perfect candidate.

## Oracle AQ in technical view ##

Okay so, Oracle AQ allows messages to be enqueued and dequeued from queues that are managed by the database.  
Where each queue is associated with a queue table. 

Each queue has a payload which can be:

- [RAW](http://www.orafaq.com/wiki/RAW)
- OBJECT: messages of specified type.
- ANYDATA: messages with any object type.

(I'm going to show you the second option)

**Queues in Oracle AQ** can be:

- **Single consumer** queues (Only one consumer is able to dequeue in one moment)
- **Multiple consumers** queues. Can be achieved in the following ways:  

      1) Multiple recipients - **recipient of a message is set before enqueing**.  
      2) Multiple subscribers - **queue has default set of subscribers**. 

**Howto work with Oracle AQ queues**

- Through JMS API, just configure WebLogic to expose [Oracle AQ queue through Foreign Server](https://docs.oracle.com/cd/E12839_01/web.1111/e13738/aq_jms.htm#JMSAD565).
- Through JDBC API (I will show it in the demo)
- Through C, .NET and many other languages.

**Important note**:  

Oracle AQ doesn't have any kind of automatic loadbalancing between subscribers like there is round robin in WebLogic JMS server.
But you can implement it. It's not that hard.

## Oracle AQ in action ##

In all parts of the demo I'm going to be using JDBC API and **attached classes AQTestObjectStruct and AQTestClient**.

### Howto create Oracle AQ table (Multiple Consumers) ###

Let's say we're going to be queuing message of type "**Message_typ**" in the database **schema ho_kloucek_in**.
So in the database run:

```
create or replace type ho_kloucek_in.Message_typ as object (
subject     VARCHAR2(30),
text        VARCHAR2(80))
```

okay, we've got an object type. Now we can create queue table with queue for it. 

```
public void createQueue() throws SQLException, AQException, ClassNotFoundException
    {
        AQQueueTableProperty qtable_prop;
        AQQueueProperty queue_prop;
        AQQueueTable q_table;
        AQQueue queue;

        java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aqsession = null;

        // Register the Oracle AQ Driver
        Class.forName("oracle.AQ.AQOracleDriver");
        try {
            AQEnqueueOption enqueueOption = new AQEnqueueOption();

            aqsession = AQDriverManager.createAQSession(aqconn);

            qtable_prop = new AQQueueTableProperty("ho_kloucek_in.Message_typ");
            qtable_prop.setMultiConsumer(true);

 			/* Creating a queue table called aq_table1 in aqjava schema: */
            q_table = aqsession.createQueueTable(queueOwner, queueTable, qtable_prop);
            System.out.println("Successfully created "+queueTable+" in "+queueOwner+" schema");

 			/* Creating a new AQQueueProperty object */
            queue_prop = new AQQueueProperty();

 			/* Creating a queue called aq_queue1 in aq_table1: */
            queue = aqsession.createQueue(q_table, queueName, queue_prop);

            queue.start(true, true);
            System.out.println("Successfully created "+queueName+" in "+queueOwner+"");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            aqsession.close();
            aqconn.close();
        }
    }
```
Notice how I called **qtable_prop.setMultiConsumer(true)**

### Multiple consumers targeted by setting message recipient list ###

Now after queue table and queue setup, let's **start consumers as recipients with name set by subsName variable**:

```
public void dequeueMessage(final String subsName) throws AQException, SQLException, ClassNotFoundException  
{
    java.sql.Connection aqconn = getOracleDataSource().getConnection();
    aqconn.setAutoCommit(false);

    AQSession aq_sess = null;

    Class.forName("oracle.AQ.AQOracleDriver");

    try 
    {
        aq_sess = AQDriverManager.createAQSession(aqconn);

        AQQueue queue;
        AQMessage message;
        AQDequeueOption deq_option;

        queue = aq_sess.getQueue(queueOwner, queueName);

        AQDequeueOption opt = new AQDequeueOption();
        opt.setConsumerName(subsName);

        while (true) {
           System.out.println("Waiting on subscription:"+subsName);
           message = queue.dequeue(opt, oracle.sql.STRUCT.class);

           if (message == null) {
               System.out.println("no messages");
           } else {
               System.out.println("Successful dequeue");

               if (message.getObjectPayload().getPayloadData() instanceof STRUCT) {
                   STRUCT popedStruct = (STRUCT) message.getObjectPayload().getPayloadData();
                   System.out.println("subject: " + popedStruct.getAttributes()[0]);
                   System.out.println("text: " + popedStruct.getAttributes()[1]);
                }

                //Commit
                aqconn.commit();
            }
        }
    }
    finally {
       aq_sess.close();
       aqconn.close();
    }
}
```

Enqueing of message, I repeat, when setting message recipient list, **we're setting the recipient before enqueing**:

```
public void enqueueMessage(String xmlMessage) throws SQLException, AQException, ClassNotFoundException {
        java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aqsession = null;

        // Register the Oracle AQ Driver
        Class.forName("oracle.AQ.AQOracleDriver");
        try {
            AQEnqueueOption enqueueOption = new AQEnqueueOption();

            aqsession = AQDriverManager.createAQSession(aqconn);
            AQQueue queue = aqsession.getQueue(queueOwner, queueName);
            AQMessage msg = queue.createMessage();

            AQMessageProperty msgProps = new AQMessageProperty();
            msgProps.setPriority(1);
            Vector recipientList = new Vector();
            AQAgent subs1 = new AQAgent("Sub2", null, 0);
            recipientList.add(subs1);
            msgProps.setRecipientList(recipientList);
            msg.setMessageProperty(msgProps);

            AQObjectPayload payload = msg.getObjectPayload();

            Object [] test_attributes = new Object[2];
            test_attributes [0] = "AsyncTask";
            test_attributes [1] = "121212666";

            StructDescriptor personDesc =
                    StructDescriptor.createDescriptor("HO_KLOUCEK_IN.MESSAGE_TYP", aqconn);

            STRUCT new_async = new STRUCT(personDesc, aqconn, test_attributes);

            payload.setPayloadData(new_async);

            queue.enqueue(enqueueOption, msg);
            aqconn.commit();
            System.out.println("Message succesfully enqueued..");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            aqsession.close();
            aqconn.close();
        }
    }
```
This method sends a message into my queue and it is **designated only for consumer with name "Sub2"!**  

Let's test it.

Start two JVMs launching previously mentioned dequeueMessage method with paramerers "Sub1" and "Sub2"...  
Use class **AQTestObjectStruct** containing dequeueMessage method. Following should appear in both JVM's:

**JVM1**:

Waiting on subscription: Sub1

**JVM2**:

Waiting on subscription: Sub2

As you can see, AQQueue.dequeue method is blocking by default. Anyway, you can also block just for a certain amount of time,  
see the docs and [AQDequeueOption](https://docs.oracle.com/cd/A84870_01/doc/server.816/a76935/oracle_a.htm#77077) 


Now launch class **AQTestClient** and previously modified method enqueueMessage sending a message 

```
            Object [] test_attributes = new Object[2];
            test_attributes [0] = "AsyncTask";
            test_attributes [1] = "11111";
```

for subscriber "Sub2":

```
            AQMessage msg = queue.createMessage();

            AQMessageProperty msgProps = new AQMessageProperty();
            msgProps.setPriority(1);
            Vector recipientList = new Vector();
            AQAgent subs2 = new AQAgent("Sub2", null, 0);
            recipientList.add(subs2);
            msgProps.setRecipientList(recipientList);
            msg.setMessageProperty(msgProps);

```
**consumer is set by the name of AQAgent**. Now **after launch of AQTestClient** following should appear JUST in the **JVM2**:

```
Successful dequeue
subject: AsyncTask
text: 11111
Waiting on subscription: Sub2
```

### Multiple consumers using Subscriptions ###

Using recipient list parameter of AQMessage, you're setting the recipients before equeing. Oracle AQ docs clearly states:

```
If a recipient list is specified during enqueue, it overrides the subscription list.  
```

So let's see **howto create queue subscriptions**...let's change the dequeue method in **AQTestObjectStruct** and start it as subscriber:

```
public void dequeueMessage(final String subsName) throws AQException, SQLException, ClassNotFoundException  
{
    java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aq_sess = null;

        Class.forName("oracle.AQ.AQOracleDriver");

        try {
            aq_sess = AQDriverManager.createAQSession(aqconn);

            AQQueue queue;
            AQMessage message;
            AQDequeueOption deq_option;

            queue = aq_sess.getQueue(queueOwner, queueName);

            // add subscription
            AQAgent subs = new AQAgent(subsName, null, 0);
            queue.removeSubscriber(subs);
            queue.addSubscriber(subs,null);


            AQDequeueOption opt = new AQDequeueOption();
            opt.setConsumerName(subsName);

            while (true) {
                System.out.println("Waiting on subscription:"+subsName);
                message = queue.dequeue(opt, oracle.sql.STRUCT.class);

                if (message == null) {
                    System.out.println("no messages");
                } else {
                    System.out.println("Successful dequeue");

                    if (message.getObjectPayload().getPayloadData() instanceof STRUCT) {
                        STRUCT popedStruct = (STRUCT) message.getObjectPayload().getPayloadData();
                        System.out.println("subject: " + popedStruct.getAttributes()[0]);
                        System.out.println("text: " + popedStruct.getAttributes()[1]);
                    }

                    //Commit
                    aqconn.commit();
                }
            }
        }
        finally {
            aq_sess.close();
            aqconn.close();
        }
    }
```
(Note: comment out the row "queue.removeSubscriber(subs)" if you're adding new subscriber)

Now you can effort to **omit the recipient list** in the message before enqueing, because **set of message targets is being set by subscriptions**. 

Again let me quote the Oracle AQ DOC:

```
You do not need to specify subscriptions for a multi-consumer queue provided that producers of messages for enqueue supply a recipient list of consumers. 
In some situations it may be desirable to enqueue a message that is targeted to a specific set of consumers rather than the default list of subscribers. 
```

And that's pretty much it! If you do not specify recipient list in the message, system will deliver message to all subscribers. If you specify recipient list
then system will deliver message to the named recipients. If you won't specify recipient list and queue won't have any subscribers(AQQueue.addSubscriber method)  
then you will end up with error:7

```
oracle.AQ.AQOracleSQLException: ORA-24033: no recipients for message
ORA-06512: at "SYS.DBMS_AQIN", line 345
ORA-06512: at line 1


	at oracle.AQ.AQOracleQueue.enqueue(AQOracleQueue.java:1267)
	at com.sachinhandiekar.oracle.aq.AQTestClient.enqueueMessage(AQTestClient.java:55)
	at com.sachinhandiekar.oracle.aq.AQTestClient.main(AQTestClient.java:84)
```

### Testing multiple subscriptions ###

First change the enqueing method in **AQTestClient**, like I said, we can effort to omit setting of any receiver:

```
public void enqueueMessage(String xmlMessage) throws SQLException, AQException, ClassNotFoundException {
    java.sql.Connection aqconn = getOracleDataSource().getConnection();
        aqconn.setAutoCommit(false);

        AQSession aqsession = null;

        // Register the Oracle AQ Driver
        Class.forName("oracle.AQ.AQOracleDriver");
        try {
            AQEnqueueOption enqueueOption = new AQEnqueueOption();

            aqsession = AQDriverManager.createAQSession(aqconn);
            AQQueue queue = aqsession.getQueue(queueOwner, queueName);
            AQMessage msg = queue.createMessage();

            AQMessageProperty msgProps = new AQMessageProperty();
            msgProps.setPriority(1);

            AQObjectPayload payload = msg.getObjectPayload();

            Object [] test_attributes = new Object[2];
            test_attributes [0] = "AsyncTask";
            test_attributes [1] = "5555";

            StructDescriptor personDesc =
                    StructDescriptor.createDescriptor("HO_KLOUCEK_IN.MESSAGE_TYP", aqconn);

            STRUCT new_async = new STRUCT(personDesc, aqconn, test_attributes);

            payload.setPayloadData(new_async);

            queue.enqueue(enqueueOption, msg);
            aqconn.commit();
            System.out.println("Message succesfully enqueued..");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            aqsession.close();
            aqconn.close();
        }
    }
```
Now start **previously mentioned dequeueMessage method with subscriptions via AQTestObjectStruct class** in two JVM's:


**JVM1**:  
Waiting on subscription: Sub1  

**JVM2**:  
Waiting on subscription: Sub2  

Now run modified **AQTestClient.enqueueMessage method without recipients** and output in both JVM's will be:

```
**JVM1**:
Waiting on subscription: Sub1
Successful dequeue
subject: AsyncTask
text: 5555
Waiting on subscription: Sub1

**JVM2**:
Waiting on subscription: Sub2
Successful dequeue
subject: AsyncTask
text: 5555
Waiting on subscription: Sub2
```

As you can see message was sent to all subscribers, because we didn't specify the recipient in the message.

## What you'll find in this repository ##

- Classes AQTestObjectStruct and AQTestClient I used in demo for experimenting.
- AQTestRaw class to see howto equeue/dequeue RAW message and create queue for that.
- AQTestObject class to see howto equeue/dequeue XML message and create queue for that.

## Summary ##

I hope I explained all the details well. I really miss on Oracle AQ some sort of default load balancing of messages. That's maybe the reason why I won't use 
it for the asynchronous task distribution between our application nodes, because I need round robin there and WebLogic JMS gives it to me for free.
But if you want to communicate with multiple applications in some sort of distributed transaction then certainly go with Oracle AQ. 

regards

Tomas